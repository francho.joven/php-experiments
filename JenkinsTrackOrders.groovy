
stage('prepare master') {
    node('master') {
        git 'https://gitlab.com/francho.joven/php-experiments.git'
    }
}
stage('prepare worker') {
    node('tracking-worker') {
        git 'https://gitlab.com/francho.joven/php-experiments.git'
    }
}


stage('track orders') {
    node('master') {
        def orderJobs = [:]
        def orderBuilds = [:]
        def orders = sh(returnStdout: true, script: 'php list_orders.php').split("\n")
        
        orders.each { order ->
            def jobKey = "track-"+order
            echo "preparing ${jobKey} - ${order}"
            orderJobs[jobKey] = trackOrder(order)
        }
        orderJobs.failFast = false
        ansiColor('xterm') {
            parallel orderJobs
        }
    }
}


def trackOrder(order) {
    return {
        def b = build(job: 'Track order', parameters: [string(name: 'id_order', value: order)], quietPeriod: 0, propagate: false) 
        
        if(b.result == 'FAILURE') {
            currentBuild.result = 'UNSTABLE'
            echo "\u274C\u001B[31m order ${order} ha FALLADO\u001B[0m"
        } else {
            echo "\u2705\u001B[32m order ${order} OK\u001B[0m"
        }
                
    }
}
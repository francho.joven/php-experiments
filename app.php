<?php

include "/etc/francho/app.conf.php";

$db_user = isset($CONFIG_DB_USER) ? $CONFIG_DB_USER : getenv('DB_USER');
$db_pass = isset($CONFIG_DB_PASS) ? $CONFIG_DB_PASS : getenv('DB_PASS');

printf("NODE: %s\n",getenv('NODE_LABELS'));
printf("DB_USER: %s\n", $db_user);
printf("DB_PASS: %s\n", $db_pass);

if($db_user=="the user" && $db_pass=="the pass") {
    printf("Autorized (using env vars)\n");
} else if($db_user=="config_db_user" && $db_pass=="config_db_pass") {
    printf("Autorized (using config vars)\n");
} else {
    throw new Exception('Invalid pass');
}

printf("Hi dude: -%s- -%s-\n",$db_user,$db_pass);

?>